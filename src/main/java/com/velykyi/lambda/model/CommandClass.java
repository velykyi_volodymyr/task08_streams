package com.velykyi.lambda.model;

public class CommandClass<command> implements Command{

    static Command command = (str) -> System.out.println("Object of class: " + str);
    public void execute(String str) {
        System.out.println("Command class command: " + str);
    }
}
