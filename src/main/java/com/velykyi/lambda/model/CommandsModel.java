package com.velykyi.lambda.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class CommandsModel {
    private static Logger logger1 = LogManager.getLogger(CommandsModel.class);


    private static Map<String, Command> commands;

    public static Map<String, Command> getCommands() {
        commands = new LinkedHashMap<String, Command>();
        Command anonymousCommand = new Command() {
            @Override
            public void execute(String str) {
                System.out.println("Anonymous class command: " + str);
            }
        };
        commands.put("lambda command", (str) -> System.out.println("Lambda command: " + str));
        commands.put("anonymous class command", anonymousCommand);
        commands.put("command class command",new CommandClass()::execute);
        commands.put("q", CommandClass.command);
        logger1.info("Map of commands was created.");
        return commands;
    }
}
