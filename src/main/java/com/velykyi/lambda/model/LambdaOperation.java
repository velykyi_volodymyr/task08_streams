package com.velykyi.lambda.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LambdaOperation {
    private static Logger logger1 = LogManager.getLogger(LambdaOperation.class);

    public static int max(int el1, int el2, int el3) {
        MathOperation max = (i1, i2, i3) -> Math.max(Math.max(i1, i2), i3);
        logger1.info("Lambda max was worked.");
        return max.operation(el1, el2, el3);
    }

    public static int average(int el1, int el2, int el3) {
        MathOperation average = (i1, i2, i3) -> (i1 + i2 + i3) / 3;
        logger1.info("Lambda average was worked.");
        return average.operation(el1, el2, el3);
    }
}
