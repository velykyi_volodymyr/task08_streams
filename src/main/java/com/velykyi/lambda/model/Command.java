package com.velykyi.lambda.model;

@FunctionalInterface
public interface Command {
    void execute(String str);
}
