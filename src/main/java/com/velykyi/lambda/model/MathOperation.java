package com.velykyi.lambda.model;

@FunctionalInterface
public interface MathOperation {
    int operation(int num1, int num2, int num3);

}
