package com.velykyi.lambda.controller;

import com.velykyi.lambda.model.*;
import com.velykyi.lambda.view.*;

import java.util.*;

public class Controller {
    public static void main(String[] args) {
        int num1 = 1;
        int num2 = 2;
        int num3 = 5;
        ViewLambda view1 = new ViewLambda();
        view1.printNumbers(num1, num2, num3);
        view1.printResult(LambdaOperation.max(num1, num2, num3), LambdaOperation.average(num1, num2, num3));
        //--------------------------------------------------------------------------------------------------------------
        String[] command = new String[2];
        Map<String, Command> commands = new LinkedHashMap<String, Command>();
        commands = CommandsModel.getCommands();
        ViewCommands view2 = new ViewCommands(commands);
        do{
            command = view2.readCommand();
            view2.printResult(command);
        }while(!command[0].toLowerCase().equals("q"));

    }
}
