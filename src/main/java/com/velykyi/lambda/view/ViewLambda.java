package com.velykyi.lambda.view;

public class ViewLambda {

    public void printResult(int max, int average){
        System.out.println("Max: " + max);
        System.out.println("Average: " + average);
    }

    public void printNumbers(int num1, int num2, int num3){
        System.out.println("Numbers: " + num1 + ", " + num2 + ", " + num3);
    }
}
