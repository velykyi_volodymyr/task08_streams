package com.velykyi.lambda.view;

import com.velykyi.lambda.model.Command;

import java.util.Map;
import java.util.Scanner;

public class ViewCommands {

    private Map<String, Command> commands;

    public ViewCommands(Map<String, Command> commands) {
        this.commands = commands;
    }

    public void printResult(String[] strs) {
        try {
            this.commands.get(strs[0]).execute(strs[1]);
        }catch (NullPointerException e){
            System.out.println("You enter no command! Please, enter one of next command: " +
                    "lambda command, anonymous class command, command class command");
        }
    }

    public String[] readCommand() {
        String[] command = new String[2];
        Scanner scann = new Scanner(System.in);
        System.out.println("Enter command name: ");
        command[0] = scann.nextLine().toLowerCase();
        System.out.println("Enter your string: ");
        command[1] = scann.nextLine();
        return command;
    }
}
