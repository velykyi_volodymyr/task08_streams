package com.velykyi.streams.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class IntegerList implements Model {
    private static Logger logger1 = LogManager.getLogger(IntegerList.class);

    public List<Integer> list;
    private int size;

    public IntegerList(int n) {
        this.list = createList(n);
    }

    @Override
    public List<Integer> createList(int n) {
        List<Integer> integerList = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            integerList.add(new Random().nextInt(100));
        }
        logger1.info("List was created.");
        return integerList;
    }

    public double averageReduce() {
        Optional<Integer> average = this.list.stream()
                .reduce(Integer::sum);
        logger1.info("Average using reduce was calculated.");
        return (double) average.get() / 10;
    }

    public double averageStream() {
        OptionalDouble average = this.list.stream()
                .mapToInt(Integer::intValue)
                .average();
        logger1.info("Average using stream was calculated.");
        return average.getAsDouble();
    }

    public int sumStream() {
        int sum = this.list.stream()
                .mapToInt(Integer::intValue)
                .sum();
        logger1.info("Sum using stream was calculated.");
        return sum;
    }

    public int sumReduce() {
        Optional<Integer> sum = this.list.stream()
                .reduce((a, b) -> a + b);
        logger1.info("Sum using reduce was calculated.");
        return sum.get();
    }

    public int min() {
        Optional<Integer> min = this.list.stream()
                .min(Comparator.comparing(Integer::intValue));
        logger1.info("Min was calculated.");
        return min.get();
    }

    public int max() {
        Optional<Integer> max = this.list.stream()
                .max(Comparator.comparing(Integer::intValue));
        logger1.info("Max was calculated.");
        return max.get();
    }

    public int countBiggerAverage() {
        int count = (int) this.list.stream()
                .filter(el -> el > this.averageStream())
                .count();
        logger1.info("Count od values bigger than average was calculated.");
        return count;
    }
}
