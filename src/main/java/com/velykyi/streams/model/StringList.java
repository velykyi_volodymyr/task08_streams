package com.velykyi.streams.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class StringList {
    private static Logger logger1 = LogManager.getLogger(StringList.class);
    List<String> list;

    public StringList(List<String> list) {
        this.list = list;
    }

    public int countOfUniqueWords() {
        int countWords = 0;
        System.out.println(this.list);
        for (String element : this.list) {
            int count = (int) this.list.stream()
                    .filter(el -> el.equals(element))
                    .count();
            if (count == 1) {
                countWords++;
            }
        }
        logger1.info("Count of unique words was calculated.");
        return countWords;
    }

    public List<String> sortUniqueList() {
        logger1.info("List of unique words was sorted.");
        return this.list.stream()
                .distinct()
                .sorted()
                .collect(Collectors.toList());
    }

    public Map<String, Long> countOfAllWords() {
        Map<String, Long> stringMap = this.list.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        logger1.info("Count of each words was calculated.");
        return stringMap;
    }

    public List<String> createSymbolList() {
        List<String> symbolList = new ArrayList<>();
        for (String el : this.list) {
            symbolList.addAll(Arrays.asList(el.split("")));
        }
        logger1.info("List of characters was created.");
        return symbolList;
    }

    public Map<String, Long> countOfChar(List<String> charList) {
        Map<String, Long> charMap = charList.stream()
                .filter(el -> !el.equals(el.toUpperCase()))
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        logger1.info("Count of characters was calculated.");
        return charMap;
    }
}
