package com.velykyi.streams.controller;

import com.velykyi.streams.model.IntegerList;
import com.velykyi.streams.model.StringList;
import com.velykyi.streams.view.ViewNumbers;
import com.velykyi.streams.view.ViewStrings;

import java.util.*;

public class Controller {
    public static void main(String[] args) {
        IntegerList integerModel = new IntegerList(10);
        ViewNumbers viewNumbers = new ViewNumbers();
        viewNumbers.printList(integerModel.list);
        double average1 = integerModel.averageReduce();
        double average2 = integerModel.averageStream();
        int sum2 = integerModel.sumReduce();
        int sum1 = integerModel.sumStream();
        int max = integerModel.max();
        int min = integerModel.min();
        int countInt = integerModel.countBiggerAverage();
        viewNumbers.printResult(average1, average2, sum1, sum2, min, max, countInt);
        //--------------------------------------------------------------------------------------------------------------
        ViewStrings viewStrings = new ViewStrings();
        StringList stringModel = new StringList(viewStrings.getData());
        int countStr = stringModel.countOfUniqueWords();
        List<String> sortedList = stringModel.sortUniqueList();
        java.util.Map<String, Long> countOfWords = stringModel.countOfAllWords();
        Map<String, Long> countOfChar = stringModel.countOfChar(stringModel.createSymbolList());
        viewStrings.printResult(countStr, sortedList, countOfWords, countOfChar);
    }
}
