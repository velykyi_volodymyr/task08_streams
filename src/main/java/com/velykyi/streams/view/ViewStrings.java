package com.velykyi.streams.view;

import java.util.*;

public class ViewStrings {
    public List<String> getData(){
        Scanner scann = new Scanner(System.in);
        List<String> stringList = new ArrayList<>();
        System.out.println("Enter your text: ");
        do{
            stringList.addAll(Arrays.asList(scann.nextLine().split(" ")));
        } while (!stringList.get(stringList.size() - 1).equals(""));
        stringList.remove("");
        return stringList;
    }

    public void printResult(int count, List<String> sortedList,
                            Map<String, Long> countOfWords, Map<String, Long> countOfChar){
        System.out.println("Count of unique words " + count);
        System.out.println("Sorted list of unique words " + sortedList);
        System.out.println("Words and amount " + countOfWords);
        System.out.println("Characters and amount " + countOfChar);
    }
}
