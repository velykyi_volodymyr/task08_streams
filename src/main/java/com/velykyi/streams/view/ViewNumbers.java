package com.velykyi.streams.view;

import java.util.*;

public class ViewNumbers {

    public void printList(List<Integer> list){
        System.out.println("Random list: ");
        for ( int el : list) {
            System.out.print(el + " ");
        }
    }

    public void printResult(double average1, double average2,
                            int sum1, int sum2, int min,
                            int max, int count){
        System.out.println("Average using reduce " + average1);
        System.out.println("Average using stream " + average2);
        System.out.println("Sum using stream " + sum1);
        System.out.println("Sum using reduce " + sum2);
        System.out.println("Max " + max);
        System.out.println("Min " + min);
        System.out.println("Count of values bigger than average " + count);
    }
}
